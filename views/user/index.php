<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [


            'username',
            [
                'attribute' => 'created_at',
                'value' => function ($model, $key, $index, $column) {
                    return date('d.m.Y H:i:s', $model->created_at);
                },
                'filter' => false
            ],
            [
                'label' => 'Balance',
                'value' => function ($model, $key, $index, $column) {

                    return \app\models\User::getBalanceById($model->id);
                },
                'filter' => false
            ],


        ],
    ]); ?>
</div>
