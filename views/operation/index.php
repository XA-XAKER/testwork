<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OperationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Operations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        My Balance: <?=\app\models\User::getMyBalance()?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'created_at',
                'value' => function ($model, $key, $index, $column) {
                    return date('d.m.Y H:i:s', $model->created_at);
                },
                'filter' => false
            ],
            [
                'attribute' => 'value',
                'value' => function ($model, $key, $index, $column) {
                    if($model->user_id == Yii::$app->user->id){
                        return $model->value;
                    }elseif($model->user_from_id == Yii::$app->user->id){
                        return -$model->value;
                    }
                },
                'filter' => false
            ],
            [
                'label' => 'From',
                'value' => function ($model, $key, $index, $column) {
                    if($model->user_id == Yii::$app->user->id){
                        return $model->userFrom->username;
                    }elseif($model->user_from_id == Yii::$app->user->id){
                        return 'Me';

                    }
                },
                'filter' => false
            ],
            [
                'label' => 'To',
                'value' => function ($model, $key, $index, $column) {
                    if($model->user_id == Yii::$app->user->id){

                        return 'Me';
                    }elseif($model->user_from_id == Yii::$app->user->id){
                        return $model->userTo->username;

                    }
                },
                'filter' => false
            ],
            // 'created_at',

        ],
    ]); ?>
</div>
