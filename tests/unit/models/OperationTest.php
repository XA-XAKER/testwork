<?php

namespace tests\models;
use app\models\LoginForm;
use app\models\Operation;
use app\models\User;


class OperationTest extends \Codeception\Test\Unit
{
    private $model;
    private $username_from;
    private $username_to;
    private $user_id_from;
    private $user_id_to;



    protected function _after()
    {
        \Yii::$app->user->logout();
        User::deleteAll(['id' => $this->user_id_from]);


    }
    protected function _before()
    {
        $this->username_from = 'test_' . uniqid();
        $this->user_id_from = User::getUserIdByUsername($this->username_from);

        $this->model = new LoginForm([
            'username' => $this->username_from,
        ]);
        $this->model->login();
    }

    public function testDonateNotUser(){
        $this->username_to = 'test_' . uniqid();
        $this->model = new Operation([
            'created_at' => time(),
            'user_from_id' => $this->user_id_from,
            'username' => $this->username_to,
            'value' => 10.01
        ]);
        expect_that($this->model->save());
        $this->user_id_to = User::find()->where(['username' => $this->username_to])->one()->id;
        expect_that(User::findOne($this->user_id_to)->bal == 10.01);
        expect_that(User::findOne($this->user_id_from)->bal == -10.01);
        User::deleteAll(['id' => $this->user_id_to]);

    }
    public function testDonateUser(){
        $this->username_to = 'test_' . uniqid();
        $this->user_id_to = User::getUserIdByUsername($this->username_to);
        $this->model = new Operation([
            'created_at' => time(),
            'user_from_id' => $this->user_id_from,
            'username' => $this->username_to,
            'value' => 10.01
        ]);
        expect_that($this->model->save());
        expect_that(User::findOne($this->user_id_to)->bal == 10.01);
        expect_that(User::findOne($this->user_id_from)->bal == -10.01);
        User::deleteAll(['id' => $this->user_id_to]);
    }


}
