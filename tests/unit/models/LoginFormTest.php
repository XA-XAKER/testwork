<?php

namespace tests\models;

use app\models\LoginForm;
use app\models\User;
use Codeception\Specify;

class LoginFormTest extends \Codeception\Test\Unit
{
    private $model;
    private $user_id;
    private $username;

    protected function _after()
    {
        \Yii::$app->user->logout();

    }


    public function testLoginNoUser()
    {
        $this->username = 'test_' . uniqid();
        $this->model = new LoginForm([
            'username' => $this->username,
        ]);


        expect_that($this->model->login());
        expect_not(\Yii::$app->user->isGuest);
        User::deleteAll("username='$this->username'");
    }


    public function testLoginUser()
    {
        $this->username = 'test_' . uniqid();
        $this->user_id = User::getUserIdByUsername($this->username);

        $this->model = new LoginForm([
            'username' => $this->username,
        ]);

        expect_that($this->model->login());
        expect_not(\Yii::$app->user->isGuest);
        User::deleteAll("id=$this->user_id");


    }

}
