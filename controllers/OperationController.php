<?php

namespace app\controllers;

use app\models\User;
use Yii;
use app\models\Operation;
use app\models\OperationSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OperationController implements the CRUD actions for Operation model.
 */
class OperationController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $searchModel = new OperationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCreate()
    {
        $model = new Operation();
        $model->created_at = time();
        $model->user_from_id = Yii::$app->user->id;


        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect('/');

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


}
