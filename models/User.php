<?php

namespace app\models;

use yii\db\ActiveRecord;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{


    public static function tableName()
    {
        return 'user';
    }


    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new \yii\base\NotSupportedException('Method findIdentityByAccessToken not implemented.');

    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->getAttribute('auth_key');
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAttribute('auth_key') === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
    public static function findByUsername($username)
    {
        return static::findOne(self::getUserIdByUsername($username));
    }
    public static function getUserIdByUsername($username){
        $user = User::find()->where(['username' => $username])->one();

        if($user){
            return $user->id;
        }else{
            $user = new User();
            $user->created_at = time();
            $user->username = $username;
            $user->save();

        }

        return $user->id;


    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setAttribute('auth_key', \Yii::$app->security->generateRandomString());
        }


        return parent::beforeSave($insert);
    }
    static function getBalanceById($id){
        $user = User::findOne($id);
        return round($user->bal, 2);
    }

    static function getMyBalance(){
        return self::getBalanceById(\Yii::$app->user->id);

    }

}
