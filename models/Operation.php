<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operation".
 *
 * @property integer $id
 * @property integer $type
 * @property double $value
 * @property integer $user_id
 * @property integer $user_from_id
 * @property integer $created_at
 */
class Operation extends \yii\db\ActiveRecord
{
    public $username;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_from_id', 'created_at'], 'integer'],
            [['value'], 'number', 'min' => 0.01],
            [['created_at', 'username', 'value'], 'required'],
            [['username'], function ($attribute, $params) {
                if (User::find()->where(['username' => $this->username, 'id' => Yii::$app->user->id])->one()) {
                    $this->addError($attribute, 'You can not donate yourself!');
                }

            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'value' => 'Sum',
            'user_id' => 'User ID',
            'user_from_id' => 'User From ID',
            'created_at' => 'Created At',
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->user_id = User::getUserIdByUsername($this->username);
            $transaction = User::getDb()->beginTransaction();
            try {
                $userTo = User::findOne($this->user_id);
                $userTo->updateCounters(['bal' => $this->value]);

                $userTo = User::findOne($this->user_from_id);
                $userTo->updateCounters(['bal' => -$this->value]);

                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function getUserFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'user_from_id']);

    }

    public function getUserTo()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);

    }


}
