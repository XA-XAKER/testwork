<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Operation;

/**
 * OperationSearch represents the model behind the search form about `app\models\Operation`.
 */
class OperationSearch extends Operation
{

    public function search($params)
    {
        $query = Operation::find();
        $myid = Yii::$app->user->id;
        $query->andWhere("user_id=$myid OR user_from_id=$myid");
        $query->orderBy("created_at DESC");


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false
        ]);

        return $dataProvider;
    }
}
