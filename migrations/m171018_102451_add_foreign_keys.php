<?php

use yii\db\Migration;

class m171018_102451_add_foreign_keys extends Migration
{
    public function safeUp()
    {
        $this->createIndex(
            'fk_user_id',
            'operation', 'user_id'
        );
        $this->addForeignKey(
            'fk_user_id',
            'operation',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'fk_user_from_id',
            'operation', 'user_from_id'
        );
        $this->addForeignKey(
            'fk_user_from_id',
            'operation',
            'user_from_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addColumn(
            'user',
            'bal',
            $this->float()->defaultValue(0)
            );




    }

    public function safeDown()
    {
        echo "m171018_102451_add_foreign_keys cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171018_102451_add_foreign_keys cannot be reverted.\n";

        return false;
    }
    */
}
