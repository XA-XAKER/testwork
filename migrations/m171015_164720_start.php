<?php

use yii\db\Migration;

class m171015_164720_start extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            'user',
            [
                'id' => $this->primaryKey(),
                'username' => $this->string(255)->notNull()->unique(),
                'auth_key' => $this->string(35),
                'created_at' => $this->integer(11)->notNull(),
            ],
            $tableOptions
        );

        $this->createTable(
            'operation',
            [
                'id' => $this->primaryKey(),
                'value' => $this->float(),
                'user_id' => $this->integer(11),
                'user_from_id' => $this->integer(11),
                'created_at' => $this->integer(11)->notNull(),
            ],
            $tableOptions
        );


    }

    public function safeDown()
    {
        echo "m171015_164720_start cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171015_164720_start cannot be reverted.\n";

        return false;
    }
    */
}
